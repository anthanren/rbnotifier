﻿using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RBNotifier
{
    class Program
    {
        static void Main(string[] args)
        {
            var url = new MongoUrl("mongodb://119.3.104.187:18018");
            var client = new MongoClient(url);
            var database = client.GetDatabase("Messages");
            IMongoCollection<Message> collection = database.GetCollection<Message>("cd56432f-ba72-49b5-99f6-32d75c72edb6");

            List<Message> Messages = GetNotifierMessages(collection);

            SendMessageAsync(Messages).GetAwaiter().GetResult();

            //UpdateNotifierMessagesStatus(collection, Messages);

        }

        private static List<Message> GetNotifierMessages(IMongoCollection<Message> collection)
        {

            var builderFilter = Builders<Message>.Filter;
            var filter = builderFilter.Eq("status", "TODO");

            var result = collection.Find<Message>(filter).ToList();
            return result;
        }

        private static bool UpdateNotifierMessagesStatus(IMongoCollection<Message> collection, List<Message> Messages)
        {
            var builderFilter = Builders<Message>.Filter;
            var filter = builderFilter.Eq("_id", Messages[0]._id);
            var update = Builders<Message>.Update.Set("status", "SENT").Set("timestamp", DateTime.Now.ToString());
            var result = collection.UpdateMany(filter, update);
            return true;
        }

        private static async Task<bool> SendMessageAsync(List<Message> Messages)
        {
            foreach (Message msg in Messages)
            {
               foreach(string to in msg.To)
                {
                    int[] channels = {1};
                    APIBody body = new APIBody();
                    //body.address = "18015500426";
                    body.address = "18601190779";
                    body.type = "sms";
                    body.content = "测试短信1502";
                    body.channels = channels;
                    body.token = "cyontoken";
                    try
                    {
                        var result = await Send(body);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }

                }
            }
            return true;
        }

        private static async Task<string> Send(APIBody body)
        {

            var payload = JsonConvert.SerializeObject(body);
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            var response = await new HttpClient().PostAsync("https://xdu-message-api.azurewebsites.net/webapi/message/post", new StringContent(payload, Encoding.UTF8, "application/json"));
            var result = await response.Content.ReadAsStringAsync();

            return result;
        }

    }
}