﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBNotifier
{    
    public class Message
    {
        /// <summary>         
        /// Message ID        
        /// </summary>       
        public ObjectId _id  { get; set; }

        /// <summary>  
        /// TO
        /// </summary>  
        public string From { get; set; }

        /// <summary>  
        /// TO
        /// </summary>  
        public string[] To { get; set; }
        /// <summary>  
        /// CC
        /// </summary>  
        public string[] Cc { get; set; }
        /// <summary>  
        /// BCC
        /// </summary>  
        public string[] Bcc { get; set; }
        /// <summary>  
        /// Subject  
        /// </summary>  
        public string Subject { get; set; }
        /// <summary>  
        /// Body  
        /// </summary>  
        public string HtmlBody { get; set; }

        /// <summary>  
        /// Body  
        /// </summary>  
        public string TextBody { get; set; }
        /// <summary>  
        /// Body  
        /// </summary>  
        public Guid InstitutionIdentity { get; set; }
        /// <summary>  
        /// Body  
        /// </summary>  
        public Guid Originator { get; set; }
        /// <summary>  
        /// Body  
        /// </summary>  
        public string status { get; set; }
        /// <summary>  
        /// Body  
        /// </summary>  
        public DateTime timestamp { get; set; }
    }
}
