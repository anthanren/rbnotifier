﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBNotifier
{
    public class APIBody
    {
        /// <summary>         
        /// address      
        /// </summary>       
        public string address { get; set; }

        /// <summary>  
        /// type
        /// </summary>  
        public string type { get; set; }

        /// <summary>  
        /// content
        /// </summary>  
        public string content { get; set; }
        /// <summary>  
        /// channels
        /// </summary>  
        public int[] channels { get; set; }
        /// <summary>  
        /// token
        /// </summary>  
        public string token { get; set; }
    }
}
